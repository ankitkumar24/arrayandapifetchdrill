
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

// API Url
const userAPI = "https://jsonplaceholder.typicode.com/users";
const todosAPI = "https://jsonplaceholder.typicode.com/todos";

const path = require("path");

//1. fetching users

const users = fetch(userAPI)
  .then((response) => response.json())
  .catch((error) => console.error('Error fetching users:', error));
users.then((users)=>{
    // console.log(users);
});

//2. fetching todos
const todos = fetch(todosAPI)
.then((response)=> response.json())
.catch((error) => console.error('Error fetching todos:',error));
todos.then((todos)=>{
    // console.log(todos);
});

//3. fetching users and todos

fetch(userAPI)
.then((response)=> response.json())
.then((users)=>{
// console.log(users);
})
.catch((error)=> console.error('Error fetching users:',error))
.then(()=> fetch(todosAPI))
.then((response)=> response.json())
.then((users)=>{
// console.log(users);
}).catch((error)=> console.error('Error fetching todos:',error));


//4. users and their details
fetch(userAPI)
.then((response)=> response.json())
.then ((users)=>{
    
  let promises = users.map((ele)=>fetch(`https://jsonplaceholder.typicode.com/users?id=${ele.id}`)
  .then ((response) => response.json()))

  return Promise.all(promises);
})

// .then((users)=> console.log(users))
.catch((error)=> console.error('Error fetching users', error));

// 5. first todo and their users
 fetch(todosAPI)
 .then((response)=> response.json())
 .then((todo)=> {
    // console.log(todo[0]);
    return todo[0];
 })
 .then((firstTodo)=>{
    let id = firstTodo.userId;
   return fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`)
   .then((response)=> response.json());
})
.then((users)=> console.log(users))
.catch((error)=> console.error('Error fetching users', error)) ;
